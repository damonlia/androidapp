package com.example.android.miwok.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.android.miwok.R;
import com.example.android.miwok.database.DataBaseHelper;

import java.util.ArrayList;

/**
 * Created by Lia on 26.01.2017.
 */

public class ListActivity extends AppCompatActivity {

    DataBaseHelper dbHelper;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);

        ListView listView = (ListView)  findViewById(R.id.list);
        final ArrayList<String> theList = new ArrayList<>();
        dbHelper = new DataBaseHelper(this);
        Cursor data = dbHelper.getListContents();

        final Intent webViewIntent = new Intent(this, WebViewActivity.class);

        if(data.getCount() == 0){
            Toast.makeText(ListActivity.this,"Jest puste",Toast.LENGTH_LONG).show();

        }else {
            while(data.moveToNext()){
                theList.add(data.getString(1));
                ListAdapter listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,theList);
                listView.setAdapter(listAdapter);
            }
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                webViewIntent.putExtra("url", theList.get(position));
                startActivity(webViewIntent);
            }
        });

    }


}
