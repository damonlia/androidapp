/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.miwok.Activity;


import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.miwok.R;
import com.example.android.miwok.database.DataBaseHelper;


public class WebsiteFragment extends Activity {

    DataBaseHelper dbHelper;
    Button btsnAdd, seeBtn;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.addwebsite);
        editText = (EditText) findViewById(R.id.editText3);
        btsnAdd = (Button) findViewById(R.id.button);
        seeBtn = (Button)  findViewById(R.id.button3);
        dbHelper = new DataBaseHelper(this);

        btsnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newEntry = editText.getText().toString();
                if(editText.length() != 0 ){
                    AddData(newEntry);
                    editText.setText("");
                }else {
                    Toast.makeText(WebsiteFragment.this, "Musisz cos wpisac", Toast.LENGTH_LONG).show();
                }

                Intent intent= new Intent();
                PendingIntent pIntent = PendingIntent.getActivity(WebsiteFragment.this, (int) System.currentTimeMillis(), intent, 0);
                Notification noti = new Notification.Builder(WebsiteFragment.this)
                        .setContentTitle("Notification")
                        .setContentText("Zapisano do bazy danych")
                        .setSmallIcon(R.drawable.ic_play_arrow)
                        .setContentIntent(pIntent).getNotification();
                noti.flags = Notification.FLAG_AUTO_CANCEL;

                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.notify(0,noti);
            }
        });

        seeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent next = new Intent(WebsiteFragment.this, ListActivity.class);
                startActivity(next);
            }
        });

    }

    public void AddData(String newEntry){
        boolean insertdata = dbHelper.addData(newEntry);


        if(insertdata ==true) {
            Toast.makeText(WebsiteFragment.this,"Poszlo",Toast.LENGTH_LONG).show();

        }else {
            Toast.makeText(WebsiteFragment.this,"Cos nie tak",Toast.LENGTH_LONG).show();
        }

    }

}
