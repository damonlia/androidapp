package com.example.android.miwok.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.android.miwok.R;

/**
 * Created by Lia on 26.01.2017.
 */

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.buttonslayout);

        final Button mainButton = (Button) findViewById(R.id.mainActivity);
        final Button youtubeButton = (Button) findViewById(R.id.ytActivity);
        final Button websitesButton = (Button) findViewById(R.id.AddListActivity);

        final Intent mainIntent = new Intent(this, SecondActivity.class);
        final Intent youtubeIntent = new Intent(this, YTActivity.class);
        final Intent websitesIntent = new Intent(this, WebsiteFragment.class);

        mainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(mainIntent);
            }
        });

        youtubeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(youtubeIntent);
            }
        });

        websitesButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startActivity(websitesIntent);
            }
        });

    }
}
