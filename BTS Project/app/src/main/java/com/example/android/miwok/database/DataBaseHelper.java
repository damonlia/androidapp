package com.example.android.miwok.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Lia on 26.01.2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String Database_Name = "dbbts.db";
    public static final String Table_Name = "websitesTable";
    public static final String COL_1 = "id";
    public static final String COL_2 = "name";
    public static final String COL_3 = "url";

    public DataBaseHelper(Context context) {
        super(context, Database_Name, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table "+ Table_Name +"( ID INTEGER PRIMARY KEY AUTOINCREMENT, url  TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS" + Table_Name);
        onCreate(sqLiteDatabase);
    }


    public boolean addData(String url){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_3,url);

        long result = db.insert(Table_Name,null, contentValues);

        if(result == -1){
            return false;
        }else{
            return  true;
        }
    }

    public Cursor getListContents(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor data = db.rawQuery("SELECT * FROM "+ Table_Name,null);
        return data;
    }


//    public Integer deleteWebsite(String websiteName){
//        SQLiteDatabase db = getWritableDatabase();
//        return db.delete(Table_Name, "url = ?", new String[] {websiteName});
//    }

}
