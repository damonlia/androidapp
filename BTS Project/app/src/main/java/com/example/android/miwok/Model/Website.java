package com.example.android.miwok.Model;

/**
 * Created by Lia on 26.01.2017.
 */

public class Website {

    private int id;
    private String name;
    private String url;

    public Website(String name, String url){
        this.url = url;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
