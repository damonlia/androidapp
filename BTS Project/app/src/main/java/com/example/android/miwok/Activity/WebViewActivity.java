package com.example.android.miwok.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.example.android.miwok.R;

/**
 * Created by Lia on 26.01.2017.
 */

public class WebViewActivity extends AppCompatActivity {
   // DataBaseHelper dataBaseHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        WebView webView = (WebView) findViewById(R.id.webviewPole);

        String url = "http://" + getIntent().getStringExtra("url");

        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(url);

//        Integer integer = dataBaseHelper.deleteWebsite(getIntent().getStringExtra("url"));
//        if(integer >0){
//            Toast.makeText(WebViewActivity.this,"Link usunięto",Toast.LENGTH_LONG).show();
//        }else {
//            Toast.makeText(WebViewActivity.this,"Cos nie tak",Toast.LENGTH_LONG).show();
//        }
    }


}
